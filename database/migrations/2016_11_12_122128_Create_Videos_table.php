<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->text('summary')->nullable();
            $table->string('source',150)->unique()->comment('URL of the video source');
            $table->enum('provider',['Y','F'])->comment('F=Facebook,Y=Youtube');
            $table->enum('display',['Y',"N"])->default('Y');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Videos');
    }
}
