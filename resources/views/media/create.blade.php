
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">


                    <div class="panel-body">
                        {!! Form::open(['url' => '/professions']) !!}
                        <div class="form-group">
                            {!! Form::label('name','Gallery Name') !!}
                            {!! Form::text('name',null,['class' =>'form-control']) !!}
                            </div>
                        <div> {!! Form::label('name','Gallery Name') !!}
                            {!! Form::textarea('description',null,['class' =>'form-control']) !!}
                        </div>


                        <div class="form-group">
                            <div class="radio-inline">
                                {!! Form::label('display','Display') !!}
                            </div>
                            {!! Form::radio('display','Y',true) !!}Yes
                            {!! Form::radio('display','N') !!}No
                            </div>

                        <div class="form-group">
                            {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
