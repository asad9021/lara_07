<table class="table">
    <thead>
    <tr>

        <th>ID</th>
        <th>Profession Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($all_profession as $data)
    <tr>

        <td>{{$data->id}}</td>
        <td>{{$data->profession_name}}</td>
        <td><a href="/create">Insert</a>
            <a href="/professions/{{$data->id}}/edit">  Edit  </a>|
            {{ Form::open([
                                           'method'=> 'DELETE',
                                           'url' => ['/professions', $data->id],
                                           ]) }}

            {{ Form::submit('Delete', ['class' => 'btn']) }}

            {{ Form::close() }}
        </td>



    </tr>
    @endforeach

    </tbody>
</table>