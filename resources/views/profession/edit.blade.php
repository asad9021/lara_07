<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">


                <div class="panel-body">
                    {!! Form::open(['method'=>'PATCH', 'url' => '/professions/'. $edit_data->id]) !!}
                    <div class="form-group">
                        {!! Form::label('Profession Name:') !!}
                        {!! Form::text('profession_name',isset($edit_data->profession_name)?$edit_data->profession_name:null,['class' =>'form-control']) !!}


                        <div class="form-group">
                            {!! Form::submit('Update', null, ['class'=> 'form-control']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>