<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable=['name','description','display'];
public function videos(){

    return $this->belongsToMany('\App\Video');
}
}
